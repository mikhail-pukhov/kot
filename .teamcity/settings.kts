import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.dockerCommand
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.sshExec
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2019_2.vcs.GitVcsRoot


/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2021.2"

project {

    vcsRoot(HttpsGithubComMikhailPukhovTestAppGitRefsHeadsMain)

    buildType(Build)
}

object Build : BuildType({
    name = "Build"

    params {
        password("dockerPassword", "123")
        password("sshkey", "zxx42be91b4740ef16f584d29f959bd40181917009da8036c30920328d46a060db952b1ce27b89596c45b6fa0ce43d8c3961adf93")
        password("dockerLogin", "mik")
    }

    vcs {
        root(HttpsGithubComMikhailPukhovTestAppGitRefsHeadsMain)
    }

    steps {
        dockerCommand {
            name = "docker img"
            commandType = build {
                source = file {
                    path = "Dockerfile"
                }
                namesAndTags = "mikkovrov/test_app:latest"
            }
        }
        dockerCommand {
            name = "docker login"
            commandType = other {
                subCommand = "login"
                commandArgs = "-u %dockerLogin% -p %dockerPassword%"
            }
        }
        dockerCommand {
            name = "docker push"
            commandType = push {
                namesAndTags = "mikkovrov/test_app:latest"
            }
        }
        sshExec {
            name = "deploy test_app"
            commands = "cd kube-prom/; kubectl delete -f manifests/test-app-dep.yaml; kubectl create -f manifests/test-app-dep.yaml"
            targetUrl = "3.141.37.253"
            authMethod = defaultPrivateKey {
                username = "ubuntu"
            }
            param("jetbrains.buildServer.sshexec.keyFile", "%sshkey%")
        }
    }

    triggers {
        vcs {
        }
    }
})

object HttpsGithubComMikhailPukhovTestAppGitRefsHeadsMain : GitVcsRoot({
    name = "https://github.com/mikhail-pukhov/test_app.git#refs/heads/main"
    url = "https://github.com/mikhail-pukhov/test_app.git"
    branch = "refs/heads/main"
    branchSpec = "refs/heads/*"
    authMethod = password {
        userName = "mikhail-pukhov"
        password = "123"
    }
})

